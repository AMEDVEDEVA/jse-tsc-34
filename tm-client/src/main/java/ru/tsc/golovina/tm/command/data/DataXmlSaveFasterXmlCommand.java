package ru.tsc.golovina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "data-xml-save";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Save data to xml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getAdminDataEndpoint().saveDataXml(session);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
