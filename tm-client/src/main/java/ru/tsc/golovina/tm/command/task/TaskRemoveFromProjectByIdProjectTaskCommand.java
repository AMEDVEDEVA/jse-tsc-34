package ru.tsc.golovina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class TaskRemoveFromProjectByIdProjectTaskCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-remove-from-project-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Removes task from project by id";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().unbindTaskById(session, projectId, taskId);
    }

}
